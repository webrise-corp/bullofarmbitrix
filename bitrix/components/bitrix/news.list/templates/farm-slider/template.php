<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?><div class="catalogue__slider">
<?if($arParams["DISPLAY_TOP_PAGER"]):?>
    <?=$arResult["NAV_STRING"]?><br />
<?endif;?>
<?foreach($arResult["ITEMS"] as $arItem):?>

    <?
    $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
    $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
    ?>
    <div class="catalogue__slide" id="<?=$this->GetEditAreaId($arItem['ID']);?>">
        <header class="catalogue__slide_header">

            <?if($arParams["DISPLAY_PICTURE"]!="N" && is_array($arItem["PREVIEW_PICTURE"])):?>
                <?if(!$arParams["HIDE_LINK_WHEN_NO_DETAIL"] || ($arItem["DETAIL_TEXT"] && $arResult["USER_HAVE_ACCESS"])):?>
                    <div class="slide__img"><img alt="" height="182" src="<?=$arItem["PREVIEW_PICTURE"]["SRC"]?>" width="185"></div>

                <?endif;?>
            <?endif?>
            <div class="cripto-icons"><img alt="" height="24" src="img/icons/bitcoin.svg" width="24"> <img alt="" height="30" src="img/icons/etherium.svg" width="12"> <img alt="" height="24" src="img/icons/lightcoin.svg" width="24"></div>
        </header>
        <div class="catalogue__slide_desc">
            <h4 class="farm-title"><?=$arItem["NAME"]?></h4>
        </div>

        <div class="farm-income">
            <div class="income__month">
                <p>Доходность<span class="purple-text"><?=$arItem["DISPLAY_PROPERTIES"]["PFOFIT"]["DISPLAY_VALUE"]?></span></p>
            </div>
            <div class="income__year">
                <p>Через год<span class="purple-text fs18">~480 000 Р *</span></p>
            </div>
        </div>
        <div class="farm-cost">
            <p>Стоимость<span class="fs18">236 000 Р </span></p><button class="buy-button">Купить</button>
        </div>
        <div class="farm-rent">
            <div class="rent__month">
                <p>Аренда<span class=" dashedBorder">23 400 Р /РјРµСЃ</span></p>
            </div>
            <div class="rent__service">
                <p>Через год<span>5 000 Р/мес</span></p>
            </div>
        </div>
        <div class="availability">
            <p class="aval"><span><img alt="" src="img/check.jpg"></span> Есть в наличии</p>
        </div>
        <?foreach($arItem["DISPLAY_PROPERTIES"] as $pid=>$arProperty):?>
            <small>
                <?=$arProperty["NAME"]?>:&nbsp;
                <?if(is_array($arProperty["DISPLAY_VALUE"])):?>
                    <?=implode("&nbsp;/&nbsp;", $arProperty["DISPLAY_VALUE"]);?>
                <?else:?>
                    <?=$arProperty["DISPLAY_VALUE"];?>
                <?endif?>
            </small><br />
        <?endforeach;?>
    </div>
<?endforeach;?>
<?if($arParams["DISPLAY_BOTTOM_PAGER"]):?>
    <br /><?=$arResult["NAV_STRING"]?>
    </div><?endif;?>