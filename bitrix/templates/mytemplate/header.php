<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
IncludeTemplateLangFile($_SERVER["DOCUMENT_ROOT"]."/bitrix/templates/".SITE_TEMPLATE_ID."/header.php");
$APPLICATION->SetTitle("");
CJSCore::Init(array("fx"));
$curPage = $APPLICATION->GetCurPage(true);
$theme = COption::GetOptionString("main", "wizard_eshop_bootstrap_theme_id", "blue", SITE_ID);
?><!DOCTYPE html>
<html xml:lang="<?=LANGUAGE_ID?>" lang="<?=LANGUAGE_ID?>">
<head>
	<meta http-equiv="X-UA-Compatible" content="IE=edge" />
	<meta name="viewport" content="user-scalable=no, initial-scale=1.0, maximum-scale=1.0, width=device-width">
	<link rel="shortcut icon" type="image/x-icon" href="<?=SITE_DIR?>favicon.ico" />
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.2.5/jquery.fancybox.min.css" />
	<?$APPLICATION->ShowHead();?>

	<?
	use Bitrix\Main\Page\Asset;


    $APPLICATION->SetAdditionalCSS(SITE_TEMPLATE_PATH."/css/libs.min.css", true);
    $APPLICATION->SetAdditionalCSS(SITE_TEMPLATE_PATH."/css/libs.min.css", true);
	$APPLICATION->SetAdditionalCSS(SITE_TEMPLATE_PATH."/css/styles.min.css", true);
//$APPLICATION->SetAdditionalCSS("/bitrix/css/main/bootstrap.css");
//$APPLICATION->SetAdditionalCSS("/bitrix/css/main/font-awesome.css");
//	CJSCore::Init(array("jquery"));
//	$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH ."/js/script.min.js");
    Asset::getInstance()->addJs("https://code.jquery.com/jquery-3.3.1.js");
	Asset::getInstance()->addJs(SITE_TEMPLATE_PATH . "/js/slick.js");
    Asset::getInstance()->addJs("https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.2.5/jquery.fancybox.min.js");
    Asset::getInstance()->addJs(SITE_TEMPLATE_PATH . "/js/jquery.inputmask.bundle.js");
    Asset::getInstance()->addJs(SITE_TEMPLATE_PATH . "/js/phone.js");
    Asset::getInstance()->addJs(SITE_TEMPLATE_PATH . "/js/phone-ru.js");
	Asset::getInstance()->addJs(SITE_TEMPLATE_PATH . "/js/count.js");
    Asset::getInstance()->addJs(SITE_TEMPLATE_PATH . "/js/input.js");
    Asset::getInstance()->addJs(SITE_TEMPLATE_PATH . "/js/mask.js");
    Asset::getInstance()->addJs(SITE_TEMPLATE_PATH . "/js/jquery.validate.js");
    Asset::getInstance()->addJs(SITE_TEMPLATE_PATH . "/js/calculator.js");
    Asset::getInstance()->addJs(SITE_TEMPLATE_PATH . "/js/script.js");

//$this->addExternalJS(SITE_TEMPLATE_PATH."/js/slick.js");
//$APPLICATION->SetAdditionalJS(SITE_TEMPLATE_PATH."/js/slick.js");
//$APPLICATION->SetAdditionalJS(SITE_TEMPLATE_PATH."/js/count.js");
	?>
	<title><?$APPLICATION->ShowTitle()?></title>
</head>
<!--<body class="bx-background-image bx-theme-<?=$theme?>" <?=$APPLICATION->ShowProperty("backgroundImage")?>>-->
<div id="panel"><?$APPLICATION->ShowPanel();?></div>
<!-- open .header -->
<!-- end html_open-index.html-->

