$(function () {
	var btc, rub, recoupment, minerIncome;
	var time = 3600,
	electricityCost = 4.04;
	var indexHeader = document.getElementById("defaultOpenHeader");

	function calculateHeader(currentRate, btn) {
		var currentMinerModal = btn.prev().find('.filter__select_active').attr('data-current');
		var minerPrice = $('[data-src="' + currentMinerModal + '"]').prev('p').find('span').html().slice(0, -2);
		var minerHashrate = $(currentMinerModal).find('.cell_hashrate').attr('data-hashrate').slice(0, -4);
		var minerPower = $(currentMinerModal).find('.cell_power').attr('data-power').slice(0, -2);

		var divident = parseInt(minerHashrate, 10) * time * currentRate;
		var divider = minerPrice * electricityCost * parseInt(minerPower, 10) * time;


		recoupment = Math.round(divident / divider);
		minerIncome = Math.round(parseInt(minerPrice, 10) / recoupment);

		// buy without service
		$('.miner-cost').html(minerPrice + ' &#8381;')
		$('.miner-recoupment').html(recoupment + ' мес.')
		$('.miner-income').html('~' + minerIncome + ' &#8381;/мес.')
		$('.miner-recoupment_year').html('~' + minerIncome * 12 + ' &#8381;');

		// buy with service
		var minerIncomeRent = minerIncome - 5000;
		$('.miner-recoupment_service').html(Math.round(minerPrice / minerIncomeRent) + ' мес.');
		$('.miner-income_service').html('~' + minerIncomeRent + ' &#8381;/мес.');
		$('.miner-recoupment_year_service').html('~' + minerIncomeRent * 12 + ' &#8381;')
	}

	$(document).on('click', '.filter__recount', function () {
		var currBtn = $(this);
		$.getJSON( "https://min-api.cryptocompare.com/data/price?fsym=BTC&tsyms=USD", function(jsonBtc) {
			btc = jsonBtc.USD;

			$.getJSON('https://www.cbr-xml-daily.ru/daily_json.js', function (jsonRub) {
				rub = jsonRub.Valute.USD.Value;
				var currentBtcRate = btc * rub;
				calculateHeader(currentBtcRate, currBtn);

			});
		});
	});

	$.getJSON( "https://min-api.cryptocompare.com/data/price?fsym=BTC&tsyms=USD", function(jsonBtc) {
		btc = jsonBtc.USD;
		var defaultBtn = $('.filter__recount_default');
		var buyBtn = $('.buy-button');
		var catalogueBlock = $('.catalogue-pre__block');

		$.getJSON('https://www.cbr-xml-daily.ru/daily_json.js', function (jsonRub) {
			rub = jsonRub.Valute.USD.Value;
			var currentBtcRate = btc * rub;

			if(indexHeader) {
				calculateHeader(currentBtcRate, defaultBtn);
				calculateSlides(currentBtcRate, buyBtn);
			}
			else {
				calculateSlides(currentBtcRate, catalogueBlock);
			}
		});
	});

	function calculateSlides(currentRate, btn) {
		btn.each(function () {
			if(indexHeader) {
				var buttonParent = $(this).parents('.catalogue__slide_desc');
				var incomeMonth = buttonParent.find('.income__month span');
				var incomeYear = buttonParent.find('.income__year span');
				var modal = $(this).attr('data-src');
				var minerPrice = $(this).prev('p').find('span').html().slice(0, -2);
				var minerHashrate = $(modal).find('.cell_hashrate').attr('data-hashrate').slice(0, -4);
				var minerPower = $(modal).find('.cell_power').attr('data-power').slice(0, -2);
				var divident = parseInt(minerHashrate, 10) * time * currentRate;
				var divider = minerPrice * electricityCost * parseInt(minerPower, 10) * time;


				recoupment = Math.round(divident / divider);
				minerIncome = Math.round(parseInt(minerPrice, 10) / recoupment);

				incomeMonth.html(minerIncome + ' &#8381;/мес.');
				incomeYear.html(minerIncome * 12 + ' &#8381;');
				$(modal).find('.js_return-rate').html(minerIncome);
				$(modal).find('.js_recoupment').html(recoupment);
				$(modal).find('.js_after-year').html(minerIncome * 12);
			}
			else {
				var incomeMonth = $(this).find('.js_return-rate');
				var incomeYear = $(this).find('.js_after-year');
				var minerPrice = $(this).find('[data-cost]').attr('data-cost');
				var minerHashrate = $(this).find('.cell_hashrate').attr('data-hashrate').slice(0, -4);
				var minerPower = $(this).find('.cell_power').attr('data-power').slice(0, -2);

				var divident = parseInt(minerHashrate, 10) * time * currentRate;
				var divider = minerPrice * electricityCost * parseInt(minerPower, 10) * time;

				recoupment = Math.round(divident / divider);
				minerIncome = Math.round(parseInt(minerPrice, 10) / recoupment);

				incomeMonth.html(minerIncome);
				incomeYear.html(minerIncome * 12);
				$(this).find('.js_recoupment').html(recoupment);
			}
		});
	};

	// Get the element with id="defaultOpen" and click on it
	if(indexHeader) {
		document.getElementById("defaultOpenHeader").click();
	};
});

function openTabHeader(evt, tabName) {
	var i, tabcontent, tablinks;
	tabcontent = document.getElementsByClassName("calculator__desc");
	for (i = 0; i < tabcontent.length; i++) {
		tabcontent[i].style.display = "none";
	}
	tablinks = document.getElementsByClassName("filter__button");
	for (i = 0; i < tablinks.length; i++) {
		tablinks[i].className = tablinks[i].className.replace(" active_btn", "");
	}
	document.getElementById(tabName).style.display = "flex";
	evt.currentTarget.className += " active_btn";
}
