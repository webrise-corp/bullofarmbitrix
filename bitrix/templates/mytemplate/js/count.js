$(function() {

	var rentBtns = $('.rent__btn');

	rentBtns.each(function () {
		var rentParent = $(this).parents('.aside__content');
		var rentModal = $(this).attr('data-src'); // modal id
		var rentCost = rentParent.find('.rent-cost'); // cost near btn
		var rentItems = rentParent.find('.items-count').attr('value'); // items at start
		var rentFinalPrice = $(rentModal).find('.final__rent-price');
		var rentFinalItems = $(rentModal).find('.final__rent-quantity');
		var rentPrice = rentCost.attr('data-rent') * rentItems;

		rentCost.html(rentPrice);
		rentFinalItems.html(rentItems);
		rentFinalPrice.html(rentPrice);
	})

	if($('.header_index').length > 0) {
		var btnParents = $('.aside__content');
		var buyBtn = [], prepayBtn = [];


		btnParents.each(function() {
			if($(this).hasClass('catalogue-pre__prepay')) {
				prepayBtn.push($(this).find('.preorder__btn'));
			}
			else {
				buyBtn.push($(this).find('.buy-button'));
			}
			return buyBtn, prepayBtn;
		});
		
		$(buyBtn).each(function() {
			var btnParent = $(this).parents('.aside__content');
			var modal = $(this).attr('data-src');
			var baseCount = btnParent.find('.items-count').attr('value');
			var cost = btnParent.find('.price__one-item');

			cost.html(cost.attr('data-cost') * baseCount);
			$(modal).find('.final__quantity').html(baseCount);
			$(modal).find('.final__price').html(cost.attr('data-cost') * baseCount);
		});

		$(prepayBtn).each(function() {
			var btnParent = $(this).parents('.aside__content');
			var modal = $(this).attr('data-src');
			var baseCount = btnParent.find('.items-count').attr('value');
			var cost = btnParent.find('.price__one-item');
			var percent = btnParent.find('.percent-count').val().slice(0, -1);

			if(percent === 0) {
				$(modal).find('.final__price').html(cost.attr('data-cost') * baseCount);
				cost.html(cost.attr('data-cost') * baseCount);
			}
			else {
				$(modal).find('.final__price').html(Math.round(cost.attr('data-cost') * baseCount * (1 - percent / 100)));
				cost.html(Math.round(cost.attr('data-cost') * baseCount * (1 - percent / 100)));
			}
			
			$(modal).find('.order__quantity .order__desc-number').html(baseCount + ' шт.');
			$(modal).find('.crossed').html(cost.attr('data-cost') * baseCount + '&#8381;');
		});
	}
	else {
		// page init and calc items cost
		var buyBtn = $('.buy-button');
		buyBtn.each(function() {
			var btnParent = $(this).parents('.aside__content');
			var modal = $(this).attr('data-src');
			var baseCount = btnParent.find('.items-count').attr('value');
			var cost = btnParent.find('.buy-cost');

			cost.html(cost.attr('data-cost') * baseCount);
			$(modal).find('.final__quantity').html(baseCount);
			$(modal).find('.final__price').html(cost.attr('data-cost') * baseCount);
		});

		var prepayBtn = $('.preorder__btn');
		prepayBtn.each(function() {
			var btnParent = $(this).parents('.aside__content');
			var modal = $(this).attr('data-src');
			var baseCount = btnParent.find('.items-count').attr('value');
			var cost = btnParent.find('.buy-pre');
			var percent = btnParent.find('.percent-count').val().slice(0, -1);
			
			if(percent === 0) {
				$(modal).find('.final__price').html(cost.attr('data-cost') * baseCount);
				cost.html(cost.attr('data-cost') * baseCount);
			}
			else {
				$(modal).find('.final__price').html(Math.round(cost.attr('data-cost') * baseCount * (1 - percent / 100)));
				cost.html(Math.round(cost.attr('data-cost') * baseCount * (1 - percent / 100)));
			}
			$(modal).find('.order__quantity .order__desc-number').html(baseCount + ' шт.');
			$(modal).find('.crossed').html(cost.attr('data-cost') * baseCount + '&#8381;');
			
		});
	}

	$(document).on('click', '.minus', function () {
	    var $input = $(this).parent().find('.items-count');
	    var count = parseInt($input.val()) - 1;
	    count = count < 1 ? 0 : count;
	    $input.attr('value', count)
	    $input.change();

	    var rentParent = $(this).parents('.aside__content');
	    var rentModal = rentParent.find('.rent__btn').attr('data-src');
	    var rentCost = rentParent.find('.rent-cost');
	    var rentFinalPrice = $(rentModal).find('.final__rent-price');
	    var rentItems = $(rentModal).find('.final__rent-quantity');
	    var rentPrice = rentCost.attr('data-rent') * $input.attr('value');

	    rentCost.html(rentPrice);
	    rentItems.html($input.attr('value'));
	    rentFinalPrice.html(rentPrice);

	    if ($(this).parents('.modal_desc').length > 0) {
	    	id = $(this).parents('.modal_desc').attr('id').replace('modal_desc','');

			var currentModal = $('#modal2'+id);
			var currentModal3 = $('#modal3'+id);

			currentModal.find('.order__desc-number').first().text(count + ' шт.');
			currentModal3.find('.order__desc-number').first().text(count + ' шт.');

			currentModal.find('#order__desc-number2').val(count);
			currentModal3.find('#order__desc-number2').val(count);
			
			var oneItemCost = $(this).parents('.modal_desc').find('.price__one-item');
			var allItemsCost = oneItemCost.attr('data-cost') * count;
			
			oneItemCost.html(allItemsCost);
			currentModal.find('.order__desc-number .crossed').html(allItemsCost + '&#8381;');
			currentModal.find('.order__desc-number .final__price').html(allItemsCost);
			
			currentModal.find('#order_sum2').val(allItemsCost);
			currentModal3.find('#order_sum2').val(allItemsCost);
			var blockParent = $(this).parents('.aside__content');

			if(blockParent.hasClass('catalogue-pre__prepay')) {
				var percent = blockParent.find('.percent-count').val().slice(0, -1);

				if(percent === 0) {
					$(currentModal).find('.order__desc-number .crossed').html(allItemsCost + '&#8381;');
					$(this).parents('.modal_desc').find('.order__desc-number .final__price').html(allItemsCost);
					oneItemCost.html(allItemsCost);
				}
				else {
					$(currentModal).find('.order__desc-number .crossed').html(allItemsCost + '&#8381;');
					$(this).parents('.modal_desc').find('.order__desc-number .final__price').html(Math.round(allItemsCost * (1 - percent / 100)));
					oneItemCost.html(Math.round(allItemsCost * (1 - percent / 100)));
				}
			}
			else {
				$(this).parents('.modal_desc').find('.order__desc-number .final__price').html(allItemsCost);
				oneItemCost.html(allItemsCost);
			}
	    }

	    else {
	    	var inputValue = $input.attr('value');
			var modalBtn = $(this).parents('.catalogue-pre__number').find('[data-fancybox]');
			var currentModal = modalBtn.attr('data-src');

			$(currentModal).find('.order__quantity .order__desc-number').html(inputValue + ' шт.');
			var blockParent = $(this).parents('.aside__content');

			var oneItemCostBuy = blockParent.find('.buy-cost');
			var oneItemCostPre = blockParent.find('.buy-pre');

			var allItemsCostCrossed = $(currentModal).find('.crossed');
			var allItemsCost = $(currentModal).find('.order__desc-number .final__price');

			if(blockParent.hasClass('catalogue-pre__prepay')) {
				var percent = blockParent.find('.percent-count').val().slice(0, -1);
				allItemsCostCrossed.html(oneItemCostPre.attr('data-cost') * $input.val() + '&#8381;');

				if(percent === 0) {
					Math.round(allItemsCost.html(oneItemCostPre.attr('data-cost') * $input.val() * (1 - percent / 100)));
					oneItemCostPre.html(oneItemCostPre.attr('data-cost') * $input.val());
				}
				else {
					allItemsCost.html(Math.round(oneItemCostPre.attr('data-cost') * $input.val() * (1 - percent / 100)));
					oneItemCostPre.html(Math.round(oneItemCostPre.attr('data-cost') * $input.val() * (1 - percent / 100)));
				}
			}
			else {
				allItemsCost.html(oneItemCostBuy.attr('data-cost') * $input.val());
				oneItemCostBuy.html(oneItemCostBuy.attr('data-cost') * $input.val())
			}
			$(currentModal).find('#order__desc-number2').val(inputValue);
			$(currentModal).find('#order__desc-number1').val(inputValue);
			$(currentModal).find('#order__desc-number4').val(inputValue);
	    }
		
	    return false;
	});

	$(document).on('click', '.plus', function () {
	    var $input = $(this).parent().find('.items-count');
	    $input.attr('value', parseInt($input.val()) + 1);
	    $input.change();

	    var rentParent = $(this).parents('.aside__content');
	    var rentModal = rentParent.find('.rent__btn').attr('data-src');
	    var rentCost = rentParent.find('.rent-cost');
	    var rentFinalPrice = $(rentModal).find('.final__rent-price');
	    var rentItems = $(rentModal).find('.final__rent-quantity');
	    var rentPrice = rentCost.attr('data-rent') * $input.attr('value');

	    rentCost.html(rentPrice);
	    rentItems.html($input.attr('value'));
	    rentFinalPrice.html(rentPrice);

	    if ($(this).parents('.modal_desc').length > 0) {
	    	id = $(this).parents('.modal_desc').attr('id').replace('modal_desc','');

			var currentModal = $('#modal2'+id); //preorder
			var currentModal3 = $('#modal3'+id); //buy

			currentModal.find('.order__quantity .order__desc-number').first().text($input.val() + ' шт.');
			currentModal3.find('.order__quantity .order__desc-number').first().text($input.val() + ' шт.');

			currentModal.find('#order__desc-number2').val($input.val());
			currentModal3.find('#order__desc-number2').val($input.val());
			currentModal.find('#order_sum2').val(allItemsCost);
			currentModal3.find('#order_sum2').val(allItemsCost);

			var oneItemCost = $(this).parents('.modal_desc').find('.price__one-item');
			var allItemsCost = oneItemCost.attr('data-cost') * $input.val();
			var blockParent = $(this).parents('.aside__content');

			if(blockParent.hasClass('catalogue-pre__prepay')) {
				var percent = blockParent.find('.percent-count').val().slice(0, -1);

				if(percent === 0) {
					$(currentModal).find('.order__desc-number .crossed').html(allItemsCost + '&#8381;');
					$(this).parents('.modal_desc').find('.order__desc-number .final__price').html(allItemsCost);
					oneItemCost.html(allItemsCost);
				}
				else {
					$(currentModal).find('.order__desc-number .crossed').html(allItemsCost + '&#8381;');
					$(this).parents('.modal_desc').find('.order__desc-number .final__price').html(Math.round(allItemsCost * (1 - percent / 100)));
					oneItemCost.html(Math.round(allItemsCost * (1 - percent / 100)));
				}
			}
			else {
				$(this).parents('.modal_desc').find('.order__desc-number .final__price').html(allItemsCost);
				oneItemCost.html(allItemsCost);
			}

			
	    }
	    else {
	    	var inputValue = $input.attr('value');
			var modalBtn = $(this).parents('.catalogue-pre__number').find('[data-fancybox]');
			var currentModal = modalBtn.attr('data-src');

			$(currentModal).find('.order__quantity .order__desc-number').html(inputValue + ' шт.');
			var blockParent = $(this).parents('.aside__content');

			var oneItemCostBuy = blockParent.find('.buy-cost');
			var oneItemCostPre = blockParent.find('.buy-pre');

			var allItemsCostCrossed = $(currentModal).find('.crossed');
			var allItemsCost = $(currentModal).find('.order__desc-number .final__price');

			if(blockParent.hasClass('catalogue-pre__prepay')) {
				var percent = blockParent.find('.percent-count').val().slice(0, -1);
				allItemsCostCrossed.html(oneItemCostPre.attr('data-cost') * $input.val() + '&#8381;');

				if(percent === 0) {
					allItemsCost.html(Math.round(oneItemCostPre.attr('data-cost') * $input.val() * (1 - percent / 100)));
					oneItemCostPre.html(oneItemCostPre.attr('data-cost') * $input.val());
				}
				else {
					allItemsCost.html(Math.round(oneItemCostPre.attr('data-cost') * $input.val() * (1 - percent / 100)));
					oneItemCostPre.html(Math.round(oneItemCostPre.attr('data-cost') * $input.val() * (1 - percent / 100)));
				}
			}
			else {
				allItemsCost.html(oneItemCostBuy.attr('data-cost') * $input.val());
				oneItemCostBuy.html(oneItemCostBuy.attr('data-cost') * $input.val())
			}
			$(currentModal).find('#order__desc-number2').val(inputValue);
			$(currentModal).find('#order__desc-number1').val(inputValue);
			$(currentModal).find('#order__desc-number4').val(inputValue);
	    }
		
	    return false;
	});

	$(document).on('click', '.minus-p', function () {
	    var $input = $(this).parent().find('input');
	    var count = parseInt($input.val()) - 10;
	    var counter = count + '%';
	    $input.attr('value', count);


	    $input.change();

	    var parent = $(this).parents('.aside__content');
	    var modal = parent.find('.preorder__btn').attr('data-src'); // modal window id
	    var items = parent.find('.items-count').attr('value');
	    var prepayCost = parent.find('.buy-pre'); // counted cost near btn
	    var prepayCostIndex = parent.find('.price__one-item');
	    var prepayPercent = $(modal).find('.prepay-number'); // percent in modal
	    var finalPrice = $(modal).find('.final__price'); // counted price in modal

	    if(counter.slice(0, -1) <= 0) {
	        $input.val('0');
	        $input.attr('value','0');
	        prepayPercent.html('0 %');
	        
	        if($('.header_index').length > 0) {
	        	prepayCostIndex.html(prepayCostIndex.attr('data-cost') * items);
				finalPrice.html(prepayCostIndex.html());
	        }
	        else {
	        	prepayCost.html(prepayCost.attr('data-cost') * items);
	        	finalPrice.html(prepayCost.html());
	        }
	    }
	    else {
	        $input.val(counter);
		    prepayPercent.html(counter);
	        
	        if($('.header_index').length > 0) {
	        	prepayCostIndex.html(Math.floor(prepayCostIndex.attr('data-cost') * items * (1 - count / 100)));
				finalPrice.html(prepayCostIndex.html());
	        }
		    else {
	        	prepayCost.html(Math.floor(prepayCost.attr('data-cost') * items * (1 - count / 100)));
		    	finalPrice.html(prepayCost.html());
		    }
	    }

	    return false;
	});

	$(document).on('click', '.plus-p', function () {
	    var $input = $(this).parent().find('input');
	    var count = parseInt($input.val()) + 10;
	    var counter = count + '%';
	    $input.attr('value', count);
	    $input.val(counter);
	    $input.change();

	    var parent = $(this).parents('.aside__content');
	    var modal = parent.find('.preorder__btn').attr('data-src'); // modal window id
	    var items = parent.find('.items-count').attr('value');
	    var prepayCost = parent.find('.buy-pre'); // counted cost near btn
	    var prepayCostIndex = parent.find('.price__one-item');
	    var prepayPercent = $(modal).find('.prepay-number'); // percent in modal
	    var finalPrice = $(modal).find('.final__price'); // counted price in modal

	    finalPrice.html(prepayCost.html());

	    if (count > 100) {
	    	$input.val('100 %');
	    	$input.attr('value', '100 %');

	    	if($('.header_index').length > 0) {
		    	prepayCostIndex.html('0');
		    	finalPrice.html('0');
		    }
		    else {
		    	prepayCost.html('0');
		    	finalPrice.html('0');
		    }
	    }
	    else {
	    	prepayPercent.html(counter);

	    	if($('.header_index').length > 0) {
		    	prepayCostIndex.html(Math.floor(prepayCostIndex.attr('data-cost') * items * (1 - count / 100)));
		    	finalPrice.html(prepayCostIndex.html());
		    }
		    else {
		    	prepayCost.html(Math.floor(prepayCost.attr('data-cost') * items * (1 - count / 100)));
		    	finalPrice.html(prepayCost.html());
		    }
	    }

	    return false;
	});

})