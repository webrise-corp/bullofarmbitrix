// open subject list
document.addEventListener('DOMContentLoaded', function () {
    var idHeader = document.getElementById('header_select');
    var idDelivery = document.getElementById('delivery_select');

    if (idHeader) {
        idHeader.addEventListener('click', function() {
            openList(idHeader, 'filter__select_list', 'filter__select');
        });
        replaceSubject(idHeader, 'filter__select_list', 'filter__select_active');
    }
    if (idDelivery) {
        idDelivery.addEventListener('click', function() {
            openList(idDelivery, 'filter__select_list', 'filter__select');
        });
        replaceSubject(idDelivery, 'filter__select_list', 'filter__select_active');
    }
})

function openList(subject, subjectList, filterWrap) {
    var list = subject.querySelector('.' + subjectList);
    var wrap = subject.querySelector('.' + filterWrap);

    subject.querySelector('img').classList.toggle('rotate180');
    list.classList.toggle('open_list');
    wrap.classList.toggle('filter__select_opened');

    if (list.style.maxHeight) {
        list.style.maxHeight = null;
    }
    else {
        list.style.maxHeight = list.scrollHeight + 30 + "px";
    }
}

// change subject
function replaceSubject(subject, subjectList, activeSubject) {
    var list = subject.querySelector('.' + subjectList);
    var declaredSubject = subject.querySelector('.' + activeSubject);
    var subjectElement = list.querySelectorAll('li');

    for (var i = 0; i < subjectElement.length; i++) {
        subjectElement[i].addEventListener('click', function () {
            declaredSubject.innerHTML = declaredSubject.innerHTML.replace(declaredSubject.innerHTML, this.innerHTML);
            var subjectAttr = this.getAttribute('data-miner');
            declaredSubject.setAttribute('data-current', subjectAttr);
        });
    }
}

//tabs 
function openTab(evt, tabName) {
    // Declare all variables
    var i, tabcontent, tablinks;

    // Get all elements with class="tabcontent" and hide them
    tabcontent = document.getElementsByClassName("tabcontent");
    for (i = 0; i < tabcontent.length; i++) {
        tabcontent[i].classList.remove('db');
    }

    // Get all elements with class="tablinks" and remove the class "active"
    tablinks = document.getElementsByClassName("tablinks");
    for (i = 0; i < tablinks.length; i++) {
        tablinks[i].className = tablinks[i].className.replace(" modal__nav_active", "");
    }

    // Show the current tab, and add an "active" class to the button that opened the tab
    document.getElementById(tabName).classList.add('db')
    evt.currentTarget.className += " modal__nav_active";
}

$(function () {
    $(".order-form").each(function(){
        $(this).validate({
            // Specify validation rules
            rules: {
              // The key name on the left side is the name attribute
              // of an input field. Validation rules are defined
              // on the right side
              firstname: "required",
              tel: "required",
              email: {
                required: true,
                // Specify that email should be validated
                // by the built-in "email" rule
                email: true
              }
            },
            // Specify validation error messages
            messages: {
              firstname: "Пожалуйста, введите ваше имя",
              tel: "Пожалуйста, введите ваш телефон",
              email: "Пожалуйста, введите ваш e-mail адрес"
            },
            // Make sure the form is submitted to the destination defined
            // in the "action" attribute of the form when valid
            submitHandler: function(form) {
                name = $(form).find('#order__name').val();
                tel = $(form).find('#order__tel').val();
                email = $(form).find('#order__email').val();
                delivery_place = $(form).find('#another_place').val();
                prod_id = $(form).find('#prod_id').val();
                count = ($(form).find('#order__desc-number2').val());
                total = ($(form).find('#order_sum2').val());
                type = ($(form).find('#type').val());
                console.log(name, tel, email, delivery_place, prod_id, count, total, type)

                BX.ajax({
                    url: "/catalog/ajax_order.php",
                    data: {
                        name: name,
                        tel: tel,
                        email: email,
                        delivery_place: delivery_place,
                        prod_id: prod_id,
                        count: count,
                        total: total,
                        type: type
                    },
                    method: 'POST',
                    dataType: 'json',
                    cache: false,
                    onsuccess: function (arResult) {
                    /* что-то делаем с результатом */
                    //BX.closeWait();
                    },
                    onfailure: function (XMLHttpRequest, textStatus) {
                        // console.log(XMLHttpRequest);
                        // console.log(textStatus);
                    }
                });
                $.fancybox.close(true);
                $('#sent').fadeIn();
            }
        });
    })

    

    $(document).on('click', '.sent__close, .sent__overlay', function() {
        $('#sent').fadeOut();
    })

    $('[data-fancybox]').fancybox({
        smallBtn : true,
        buttons: false
    })

    $('.catalogue__slider').slick({
        slidesToShow: 3,
        slidesToScroll: 3,
        appendArrows: $('.catalogue-index__nav'),
        responsive: [
        {
            breakpoint: 1024,
            settings: {
              centerMode: true,
              slidesToShow: 1,
              variableWidth: true
          }
        }
      ]
    })

    if($(window).width() < 767) {
        $('.features__blocks').slick({
            dots: true,
            slidesToShow: 1,
            centerMode: true,
            appendArrows: false,
            variableWidth: true,
            infinite: false
        })

        $('.news__blocks_slick').slick({
            dots: true,
            slidesToShow: 1,
            centerMode: true,
            appendArrows: false,
            variableWidth: true,
            infinite: true
        })

        $('.news__blocks').slick({
            dots: true,
            slidesToShow: 1,
            centerMode: true,
            appendArrows: false,
            variableWidth: true,
            infinite: true
        })
    }

});