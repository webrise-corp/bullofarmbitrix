<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Title");
?><?

use Bitrix\Main;
use Bitrix\Main\Context;
define("NO_KEEP_STATISTIC", true); 
define("NOT_CHECK_PERMISSIONS", true); 
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");

$APPLICATION->RestartBuffer();
Bitrix\Main\Loader::includeModule('sale');
Bitrix\Main\Loader::includeModule('catalog');
//$data = json_decode($_POST);
$request = Context::getCurrent()->getRequest();
$values = $request->getPost();
//var_dump(($_POST));
//die();

$user = new CUser;
$arFields = Array(
  "NAME"              => $_POST['name'],
  "LAST_NAME"         => "",
  "EMAIL"             => $_POST['email'],
  "LOGIN"             => generateRandomString(6),
  "LID"               => "ru",
  "ACTIVE"            => "Y",
  "GROUP_ID"          => array(10,11),
  "PASSWORD"          => "123456",
  "CONFIRM_PASSWORD"  => "123456",
  "PERSONAL_PHONE"    => $_POST['tel']
);

$ID = $user->Add($arFields);
if (intval($ID) > 0)
    echo "Пользователь успешно добавлен.";
else
    echo $user->LAST_ERROR;
$products = array(
	array('PRODUCT_ID' => intval($_POST['prod_id']), 'NAME' => 'Товар 1', 'PRICE' => intval($_POST['total'])/intval($_POST['count']), 'CURRENCY' => 'RUB', 'QUANTITY' => intval($_POST['count']))
            );

$basket = Bitrix\Sale\Basket::create(SITE_ID);

foreach ($products as $product)
    {
        $item = $basket->createItem("catalog", $product["PRODUCT_ID"]);
        unset($product["PRODUCT_ID"]);
        $item->setFields($product);
    }
    
$order = Bitrix\Sale\Order::create(SITE_ID, $ID);
$order->setPersonTypeId(1);
$order->setBasket($basket);
//echo $order->id;



$shipmentCollection = $order->getShipmentCollection();
$shipment = $shipmentCollection->createItem(
        Bitrix\Sale\Delivery\Services\Manager::getObjectById(1)
    );

$shipmentItemCollection = $shipment->getShipmentItemCollection();

/** @var Sale\BasketItem $basketItem */

foreach ($basket as $basketItem)
    {
   		$item = $shipmentItemCollection->createItem($basketItem);
        $item->setQuantity($basketItem->getQuantity());
    }

$paymentCollection = $order->getPaymentCollection();
$payment = $paymentCollection->createItem(
        Bitrix\Sale\PaySystem\Manager::getObjectById(1)
    );
$payment->setField("SUM", $order->getPrice());
$payment->setField("CURRENCY", $order->getCurrency());
    
$result = $order->save();
//echo $order->getId();
//$res = CSaleOrderPropsValue::Update(20, array("ORDER_ID"=>$order->getId(), "VALUE"=>'TYPE_BUY'));
//$res = CSaleOrderPropsValue::GetOrderProps($order->getId());
//$APPLICATION->RestartBuffer();
if ($arProp = CSaleOrderProps::GetList(array(), array('CODE' => "ORDER_TYPE"))->Fetch()) {

      //добавляем свойству CUSTOM_CITY в заказе $order_id значение нашего города - $cityVal
      $res = CSaleOrderPropsValue::Add(array(
         'NAME' => $arProp['NAME'],
         'CODE' => $arProp['CODE'],
         'ORDER_PROPS_ID' => $arProp['ID'],
         'ORDER_ID' => $order->getId(),
         'VALUE' => $_POST['type'],
      ));
   }

	var_dump($res);
    if (!$result->isSuccess())
        {
            //$result->getErrors();
        }

function generateRandomString($length = 10) {
    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $charactersLength = strlen($characters);
    $randomString = '';
    for ($i = 0; $i < $length; $i++) {
        $randomString .= $characters[rand(0, $charactersLength - 1)];
    }
    return $randomString;
}
//require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/epilog_after.php");
?><?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>