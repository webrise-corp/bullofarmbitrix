<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Title");
?><!-- start content_catalogue-pre.html-->
<main class="main-catalogue-pre">
    <div class="container">
        <h2 class="page__title" data-number='5'>О нас</h2>
    </div>

    <section class="catalogue-pre">
        <div class="container">
            <div class="catalogue-pre__block">
                <div class="catalogue-pre__block-main">
                    <div class="catalogue-pre__title">
                        <h2>Ebit Miner E9 Plus - 9 TH/s</h2>
                        <img src="" alt="" class="crypto-icon">
                        <img src="" alt="" class="crypto-icon">
                        <img src="" alt="" class="crypto-icon">
                    </div>
                    <div class="catalogue-pre__product">
                        <img src="" alt="" class="catalogue-pre__product-img">
                        <p class="catalogue-pre__product-desc"></p>
                    </div>
                    <div class="catalogue-pre__table">
                        <table>
                            <tbody>
                            <tr>
                                <td>Алгоритм хэширования</td>
                                <td>SHA-256 (подходит для майнинга Bitcoin (BTC), Peercoin (PPC), eMark (DEM) и других криптовалют.)</td>
                            </tr>
                            <tr>
                                <td>Общий хэшрейт</td>
                                <td>14 TH/s ±5%</td>
                            </tr>
                            <tr>
                                <td>Вид чипа</td>
                                <td>BM1387</td>
                            </tr>
                            <tr>
                                <td>Техпроцесс</td>
                                <td>16nm</td>
                            </tr>
                            <tr>
                                <td>Количество чипов</td>
                                <td>189 шт</td>
                            </tr>
                            <tr>
                                <td>Условия работы</td>
                                <td>0 °C to 40 °C</td>
                            </tr>
                            <tr>
                                <td>Энергоэффективность</td>
                                <td>0.1 W/GH/s</td>
                            </tr>
                            <tr>
                                <td>Энергопотребление</td>
                                <td>1320W +10% при использовании</td>
                            </tr>
                            <tr>
                                <td>Рабочая частота по умолчанию</td>
                                <td>600МГц</td>
                            </tr>
                            <tr>
                                <td>Интерфейс сетевого подключения</td>
                                <td>Ethernet</td>
                            </tr>
                            <tr>
                                <td>Охлаждение</td>
                                <td>2 вентилятора 12038 фирменного блока питания Bitmain APW3 1600W</td>
                            </tr>
                            <tr>
                                <td>Напряжение</td>
                                <td>11.60 ~13.00В</td>
                            </tr>
                            <tr>
                                <td>Блок питания</td>
                                <td>Блок питания входит в комплект Bitmain APW3 1600W</td>
                            </tr>
                            <tr>
                                <td>Габаритные размеры устройства</td>
                                <td>350мм (длина) x 135мм (ширина) x 158мм (высота)</td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="catalogue-pre__block-aside">

                </div>
            </div>
        </div>
    </section>

    <section class="catalogue-pre__pension">
        <div class="container">
            <div class="header__pros">
                <div class="pension">
                    <div class="pension__desc">
                        <h4>Отличная прибавка к пенсии</h4>
                        <p>Майнер PANTECH SX6, приносит 56 000 000 Р в месяц</p>
                        <a href="#">Подробнее</a>
                    </div>
                    <img src="img/b1.png" width="236" height="235" alt="" class="pension__img">
                </div>
                <div class="tarifs">
                    <h4>Куй пока горячо</h4>
                    <p>Снижение тарифов на электричество</p>
                    <img src="img/b2.png" alt="" width="100" height="138" class="tarifs__img">
                </div>
                <div class="tablet">
                    <h4>Планшет в подарок</h4>
                    <p>При аренде оборудования от 50 000 Р/мес. Планшет со всеми необходимыми программами в подарок</p>
                    <img src="img/b3.png" alt="" width="168" height="84" class="tablet__img">
                </div>
            </div>
        </div>
    </section>

    <section class="provider">
        <div class="container">
            <h2>Надёжный поставщик</h2>
            <div class="provider__desc">
                <div class="provider__text">
                    <p >Мы официальные поставщики оборудования компании Bitmain.</p>
                    <p >Работаем с сентября 2017 года.</p>
                    <p >За это время в Россию мы привезли и продали 512 ферм Bitmain разной модификации.</p>
                </div>

                <div class="provider__serts">
                    <img src="img/sert-01.jpg" alt="">
                    <img src="img/sert-02.jpg" alt="">
                    <img src="img/sert-03.jpg" alt="">
                    <img src="img/sert-04.jpg" alt="">
                    <img src="img/sert-05.jpg" alt="">
                </div>
            </div>
        </div>
    </section>

    <section class="warranty">
        <div class="container">
            <h2>Гарантийное обслуживание</h2>
            <div class="warranty__blocks">
                <div class="warranty__block">
                    <div class="warranty__number">
                        <span>1</span><img src="img/arrow.jpg" alt="">
                    </div>

                    <div class="warranty__text">
                        <p >Оставляете заявку <br> на ремонт</p>
                    </div>
                </div>

                <div class="warranty__block">
                    <div class="warranty__number">
                        <span>2</span><img src="img/arrow.jpg" alt="">
                    </div>

                    <div class="warranty__text">
                        <p >Оператор <br> связывается с вами</p>
                    </div>
                </div>

                <div class="warranty__block">
                    <div class="warranty__number">
                        <span>3</span><img src="img/arrow.jpg" alt="">
                    </div>

                    <div class="warranty__text">
                        <p >Курьер забирает <br> оборудование</p>
                    </div>
                </div>

                <div class="warranty__block">
                    <div class="warranty__number">
                        <span>4</span>
                    </div>

                    <div class="warranty__text">
                        <p >Через 3 дня вы получаете <br> отремонтированное оборудование</p>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="delivery">
        <div class="container">
            <h2>Доставка</h2>
            <div class="delivery__blocks">
                <div class="delivery__block">
                    <p class="fs13">Куда доставить</p>

                    <form class="delivery__choise" id="where_deliver">
                        <input id="to_techpark" type="radio" name="where_deliver" checked>
                        <label for="to_techpark">В технопарк</label>

                        <input id="another_place" type="radio" name="where_deliver">
                        <label for="another_place">В другое место</label>
                    </form>
                </div>

                <div class="delivery__block">
                    <p class="fs13">Компания</p>

                    <form class="delivery__choise" id="how_deliver">
                        <input id="ems" type="radio" name="how_deliver" checked>
                        <label for="ems">EMS</label>

                        <input id="dhl" type="radio" name="how_deliver">
                        <label for="dhl">DHL</label>
                    </form>
                </div>

                <div class="delivery__block filter__select_wrap" id="delivery_select">
                    <p class="fs13">Ферма</p>

                    <div  class="filter__select">
                        <span class="filter__select_active">Майнер PANTECH WX6 — 230 000 Р</span>
                        <img src="img/dropdown.jpg" width="9" height="5" alt="">
                    </div>

                    <ul class="filter__select_list">
                        <li>Майнер PANTECH WX6 — 230 000 Р</li>
                        <li>Майнер PANTECH WX7 — 460 000 Р</li>
                        <li>Майнер PANTECH WX8 — 690 000 Р</li>
                        <li>Майнер PANTECH WX9 — 920 000 Р</li>
                        <li>Майнер PANTECH WX0 — 100 500 Р</li>
                    </ul>
                </div>

                <div class="delivery__block delivery__count">
                    <button class=" filter__recount">Пересчитать</button>
                    <span class="final__cost">5 531 Р</span>
                </div>
            </div>
        </div>
    </section>
<!-- start contacts.html-->
<section class="footer__contacts">
    <div class="container">
        <div class="contacts">
            <div class="container__footer">
                <h2 class="contacts__title">Контакты</h2>

                <div class="contacts__block">
                    <img src="img/phone-call.svg" alt="">

                    <div>
                        <p>Единый телефон</p>
                        <p class="contacts__block_phone">8 800 123 45 67</p>
                    </div>
                </div>

                <div class="contacts__block">
                    <img src="img/telegram.svg" alt="">

                    <div>
                        <p>Телеграм канал</p>
                        <a class="contacts__block_telegram" href="#">@bullofarm</a>
                    </div>
                </div>

                <div class="contacts__block">
                    <img src="img/envelope.svg" alt="">

                    <div>
                        <p>E-mail</p>
                        <a class="contacts__block_email" href="mailto:info@bullofarm.ru">info@bullofarm.ru</a>
                    </div>
                </div>
            </div>
        </div>

        <div class="address">
            <div class="container__footer">
                
            </div>
            <div class="address__city">
                <h4 class="address__city_title">Иркутск</h4>
                <p class="address__city_address">г.Усолье-Сибирское, <br> ул. Дзержинского, 1</p>
                <iframe class="address__city_map" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2415.745891189998!2d103.66358131581553!3d52.73677767985574!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x5d070dc970d59d43%3A0xd1b9ca7c458c9acf!2z0YPQuy4g0JTQt9C10YDQttC40L3RgdC60L7Qs9C-LCAxLCDQo9GB0L7Qu9GM0LUt0KHQuNCx0LjRgNGB0LrQvtC1LCDQmNGA0LrRg9GC0YHQutCw0Y8g0L7QsdC7Liwg0KDQvtGB0YHQuNGPLCA2NjU0NjA!5e0!3m2!1sru!2sua!4v1516976954665" width="353" height="310" frameborder="0" style="border:0" allowfullscreen></iframe>
            </div>

            <div class="address__city">
                <h4 class="address__city_title">Москва</h4>
                <p class="address__city_address">Москва, ул. Новозаводская, <br> д. 8/8, к. 5 пом. 11</p>
                <iframe class="address__city_map" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2245.382175926856!2d37.505835316054565!3d55.751862999531674!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x46b5495c54c0a319%3A0x7386f9bd7b8f78f8!2z0J3QvtCy0L7Qt9Cw0LLQvtC00YHQutCw0Y8g0YPQuy4sIDgvOCwg0JzQvtGB0LrQstCwLCDQoNC-0YHRgdC40Y8sIDEyMTA4Nw!5e0!3m2!1sru!2sua!4v1516977040009" width="353" height="310" frameborder="0" style="border:0" allowfullscreen></iframe>
            </div>
        </div>
    </div>
</div>
</section>
<!-- end contacts.html-->
</main>
<!-- end content_catalogue-pre.html--><?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>