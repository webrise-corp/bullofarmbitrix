<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("");
?><div class="modal_desc" id="modal_desc1">
        <div class="catalogue-pre__block">
            <div class="catalogue-pre__block-main">
                <div class="catalogue-pre__block-title">
                    <h2>Ebit Miner E9 Plus - 9 TH/s</h2>
                    <div class="crypto-img"><img alt="" class="crypto-icon" src="img/icons/bitcoin.svg"> <img alt="" class="crypto-icon" src="img/icons/etherium.svg"> <img alt="" class="crypto-icon" src="img/icons/lightcoin.svg"></div>
                </div>
                <div class="catalogue-pre__product">
                    <div class="product__img">
                        <img alt="" class="catalogue-pre__product-img" height="115" src="img/farm-02.png" width="190">
                    </div>
                    <p class="catalogue-pre__product-desc">Востребованный аппарат для майнинга BITCOIN, который еще продолжительное время будет популярным, особенно учитывая постоянно растущие темпы роста курса BITCOIN. </p>
                </div>
                <div class="catalogue-pre__table">
                    <div class="table">
                        <div class="cell">
                            Алгоритм хэширования
                        </div>
                        <div class="cell-dot"></div>
                        <div class="cell width450">
                            SHA-256 (подходит для майнинга Bitcoin (BTC), Peercoin (PPC), eMark (DEM) и других криптовалют.)
                        </div>
                    </div>
                    <div class="table">
                        <div class="cell">
                            Общий хэшрейт
                        </div>
                        <div class="cell-dot"></div>
                        <div class="cell width450">
                            14 TH/s ±5%
                        </div>
                    </div>
                    <div class="table">
                        <div class="cell">
                            Вид чипа
                        </div>
                        <div class="cell-dot"></div>
                        <div class="cell width450">
                            BM1387
                        </div>
                    </div>
                    <div class="table">
                        <div class="cell">
                            Техпроцесс
                        </div>
                        <div class="cell-dot"></div>
                        <div class="cell width450">
                            16nm
                        </div>
                    </div>
                    <div class="table">
                        <div class="cell">
                            Количество чипов
                        </div>
                        <div class="cell-dot"></div>
                        <div class="cell width450">
                            189 шт
                        </div>
                    </div>
                    <div class="table">
                        <div class="cell">
                            Условия работы
                        </div>
                        <div class="cell-dot"></div>
                        <div class="cell width450">
                            0 °C to 40 °C
                        </div>
                    </div>
                    <div class="table">
                        <div class="cell">
                            Энергоэффективность
                        </div>
                        <div class="cell-dot"></div>
                        <div class="cell width450">
                            0.1 W/GH/s
                        </div>
                    </div>
                    <div class="table">
                        <div class="cell">
                            Энергопотребление
                        </div>
                        <div class="cell-dot"></div>
                        <div class="cell width450">
                            1320W +10% при использовании
                        </div>
                    </div>
                    <div class="table">
                        <div class="cell">
                            Рабочая частота по умолчанию
                        </div>
                        <div class="cell-dot"></div>
                        <div class="cell width450">
                            600МГц
                        </div>
                    </div>
                    <div class="table">
                        <div class="cell">
                            Интерфейс сетевого подключения
                        </div>
                        <div class="cell-dot"></div>
                        <div class="cell width450">
                            Ethernet
                        </div>
                    </div>
                    <div class="table">
                        <div class="cell">
                            Охлаждение
                        </div>
                        <div class="cell-dot"></div>
                        <div class="cell width450">
                            2 вентилятора 12038 фирменного блока питания Bitmain APW3 1600W
                        </div>
                    </div>
                    <div class="table">
                        <div class="cell">
                            Напряжение
                        </div>
                        <div class="cell-dot"></div>
                        <div class="cell width450">
                            11.60 ~13.00В
                        </div>
                    </div>
                    <div class="table">
                        <div class="cell">
                            Блок питания
                        </div>
                        <div class="cell-dot"></div>
                        <div class="cell width450">
                            Блок питания входит в комплект Bitmain APW3 1600W
                        </div>
                    </div>
                    <div class="table">
                        <div class="cell">
                            Габаритные размеры устройства
                        </div>
                        <div class="cell-dot"></div>
                        <div class="cell width450">
                            350мм (длина) x 135мм (ширина) x 158мм (высота)
                        </div>
                    </div>
                </div>
            </div>

            <div class="catalogue-pre__block-nav">
                <button class="modal__nav tablinks modal__nav_active" onclick="openTab(event, 'order1')">Заказ</button>
                <button class="modal__nav tablinks" onclick="openTab(event, 'return1')">Доходность</button>
                <button class="modal__nav tablinks" onclick="openTab(event, 'specs1')">Характеристики</button>
            </div>

            <div class="catalogue-pre__block-aside">
                <div class="aside__content tabcontent db" id="order1">
                    <div class="catalogue-pre__aval">
                        <div class="in-stock">
                            <img alt="" src="img/check.jpg">
                            <p>В наличии в Москве</p>
                        </div>
                        <div class="not-in-stock ">
                            <p>Нет в наличии</p>
                            <p class="greytext fs13">Доставка в Москву — 21 день</p>
                            <p class="greytext fs13">Доставка в Иркутск — 17 дней</p>
                        </div>
                    </div>
                    <div class="catalogue-pre__number">
                        <div class="catalogue-pre__number-preorder">
                            <div class="quantity">
                                <p class="fs13">Количество</p>
                                <div class="catalogue-pre__number-count">
                                    <div class="minus"><img alt="" src="img/minus.png"></div><input type="text" value="1">
                                    <div class="plus"><img alt=""  src="img/plus.png"></div>
                                </div>
                            </div>
                            <div class="prepay">
                                <p class="fs13">Предоплата</p>
                                <div class="catalogue-pre__number-count">
                                    <div class="minus-p"><img alt="" src="img/minus.png"></div><input type="text" value="1%">
                                    <div class="plus-p"><img alt="" src="img/plus.png"></div>
                                </div>
                            </div>
                        </div>

                        <div class="catalogue-pre__number-buy">
                            <div class="catalogue-pre__buy">
                                <div class="catalogue-pre__buy-cost">
                                    <p class="fs13">Покупка</p>
                                    <p class="catalogue-pre__number-price">236 000 <sup class="fs13">Р</sup></p>
                                </div>
                                <a class="buy-button" data-fancybox data-src="#modal2" href="javascript:;">Купить</a>
                            </div>

                            <div class="catalogue-pre__rent">
                                <div class="catalogue-pre__buy-cost">
                                    <p class="fs13">Аренда</p>
                                    <p class="catalogue-pre__number-price">23 400 <sup class="fs13">Р/МЕС</sup></p>
                                </div>
                                <button class="rent__btn">Арендовать</button>
                            </div>
                        </div>
                    </div>
                    <div class="catalogue-pre__cost">
                        <p class="fs13">Стоимость</p>
                        <div class="catalogue-pre__cost-content">
                            <div class="price">
                                <p class="fs13 greytext">274 000 Р</p>
                                <p class="price__number">236 000 <sup class="fs13">Р</sup></p>
                            </div>
                            <div class="preorder">
                                <a class="preorder__btn" data-fancybox data-src="#modal1" href="javascript:;">Предзаказать</a>

                                <div class="modal" id="modal1">
                                    <div class="modal__overlay"></div>

                                    <div class="container modal__content">
                                        <div class="modal__order">
                                            <h2>Предзаказ</h2>

                                            <form class="order-form">
                                                <label class="order__row">
                                                    <span class="fs13 label">Ваше имя</span>
                                                    <input type="text" class="text-input" id="order__name">
                                                </label>

                                                <label class="order__row">
                                                    <span class="fs13 label">Телефон</span>
                                                    <input type="text" class="text-input tel-input" id="order__tel">
                                                </label>

                                                <label class="order__row">
                                                    <span class="fs13 label">E-mail</span>
                                                    <input type="email" class="text-input" id="order__email">
                                                </label>

                                                <div class="order__delivery order__row">
                                                    <p class="fs13 label">Куда доставить</p>

                                                    <div class="delivery__choise">
                                                        <input id="to_techpark" type="radio" name="where_deliver" checked>
                                                        <label for="to_techpark">В технопарк</label>

                                                        <input class="another_place" id="another_place" type="text" placeholder="В другое место" name="where_deliver">

                                                    </div>
                                                </div>

                                                <div class="order__company order__row">
                                                    <p class="fs13 label">Компания</p>

                                                    <div class="delivery__choise">
                                                        <input id="ems" type="radio" name="how_deliver" checked>
                                                        <label for="ems">EMS</label>

                                                        <input id="dhl" type="radio" name="how_deliver">
                                                        <label for="dhl">DHL</label>
                                                    </div>
                                                </div>

                                                <div class="order__address order__row">
                                                    <p class="fs13 label">Адрес</p>
                                                    <p class="fs13">г.Усолье-Сибирское, ул. Дзержинского, 1</p>
                                                </div>

                                                <input class="send-request" type="submit" value="Отправить заявку">
                                            </form>
                                        </div>

                                        <div class="modal__order-desc">
                                            <h3 class="modal__mine-name">Ebit Miner E9 Plus - 9 TH/s</h3>

                                            <div class="order__desc">
                                                <div class="order__quantity">
                                                    <p class="fs13 greytext">Количество</p>
                                                    <p class="order__desc-number">4 шт.</p>
                                                </div>

                                                <div class="order__prepay">
                                                    <p class="fs13 greytext">Предоплата</p>
                                                    <p class="order__desc-number">60%</p>
                                                </div>

                                                <div class="order__total-cost">
                                                    <p class="fs13 greytext">Итоговая стоимость</p>
                                                    <p class="order__desc-number"><span class="fs18 crossed">1 536 000 Р</span><br>1 236 000 Р</p>
                                                </div>
                                            </div>

                                            <div class="order__instructions">
                                                <p>После отправки с вами свяжется наш оператор и уточнит детали заказа.</p>
                                                <p class="purple-text">Обращаем ваше внимание, предоплата вносится в течении 7 дней после подписания договора.</p>
                                            </div>
                                        </div>
                                        <!-- <button class="modal__close">
                                            <img src="img/cross.png" width="14" height="14" alt="">
                                        </button> -->
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="catalogue-pre__service">
                        <p class="fs13">Обслуживание</p>
                        <p class="catalogue-pre__service-rent">5 000 <sup class="fs13">Р/МЕС</sup></p>
                    </div>
                </div>
                <div class="catalogue-pre__return tabcontent" id="return1">
                    <div class="return-rate">
                        <p class="fs13">Доходность</p>
                        <p class="return-rate__number">~43 234 <sup class="fs13">Р/МЕС</sup></p>
                    </div>
                    <div class="recoupment">
                        <p class="fs13">Окупаемость</p>
                        <p class="recoupment__number">~6 <sup class="fs13">МЕС</sup></p>
                    </div>
                    <div class="after-year">
                        <p class="fs13">Через год</p>
                        <p class="after-year__number">~480 000 <sup class="fs13">Р</sup></p>
                    </div>
                </div>

                <div class="catalogue-pre__table tabcontent" id="specs1">
                    <div class="table">
                        <div class="cell">
                            Алгоритм хэширования
                        </div>
                        <div class="cell-dot"></div>
                        <div class="cell width450">
                            SHA-256 (подходит для майнинга Bitcoin (BTC), Peercoin (PPC), eMark (DEM) и других криптовалют.)
                        </div>
                    </div>
                    <div class="table">
                        <div class="cell">
                            Общий хэшрейт
                        </div>
                        <div class="cell-dot"></div>
                        <div class="cell width450">
                            14 TH/s ±5%
                        </div>
                    </div>
                    <div class="table">
                        <div class="cell">
                            Вид чипа
                        </div>
                        <div class="cell-dot"></div>
                        <div class="cell width450">
                            BM1387
                        </div>
                    </div>
                    <div class="table">
                        <div class="cell">
                            Техпроцесс
                        </div>
                        <div class="cell-dot"></div>
                        <div class="cell width450">
                            16nm
                        </div>
                    </div>
                    <div class="table">
                        <div class="cell">
                            Количество чипов
                        </div>
                        <div class="cell-dot"></div>
                        <div class="cell width450">
                            189 шт
                        </div>
                    </div>
                    <div class="table">
                        <div class="cell">
                            Условия работы
                        </div>
                        <div class="cell-dot"></div>
                        <div class="cell width450">
                            0 °C to 40 °C
                        </div>
                    </div>
                    <div class="table">
                        <div class="cell">
                            Энергоэффективность
                        </div>
                        <div class="cell-dot"></div>
                        <div class="cell width450">
                            0.1 W/GH/s
                        </div>
                    </div>
                    <div class="table">
                        <div class="cell">
                            Энергопотребление
                        </div>
                        <div class="cell-dot"></div>
                        <div class="cell width450">
                            1320W +10% при использовании
                        </div>
                    </div>
                    <div class="table">
                        <div class="cell">
                            Рабочая частота по умолчанию
                        </div>
                        <div class="cell-dot"></div>
                        <div class="cell width450">
                            600МГц
                        </div>
                    </div>
                    <div class="table">
                        <div class="cell">
                            Интерфейс сетевого подключения
                        </div>
                        <div class="cell-dot"></div>
                        <div class="cell width450">
                            Ethernet
                        </div>
                    </div>
                    <div class="table">
                        <div class="cell">
                            Охлаждение
                        </div>
                        <div class="cell-dot"></div>
                        <div class="cell width450">
                            2 вентилятора 12038 фирменного блока питания Bitmain APW3 1600W
                        </div>
                    </div>
                    <div class="table">
                        <div class="cell">
                            Напряжение
                        </div>
                        <div class="cell-dot"></div>
                        <div class="cell width450">
                            11.60 ~13.00В
                        </div>
                    </div>
                    <div class="table">
                        <div class="cell">
                            Блок питания
                        </div>
                        <div class="cell-dot"></div>
                        <div class="cell width450">
                            Блок питания входит в комплект Bitmain APW3 1600W
                        </div>
                    </div>
                    <div class="table">
                        <div class="cell">
                            Габаритные размеры устройства
                        </div>
                        <div class="cell-dot"></div>
                        <div class="cell width450">
                            350мм (длина) x 135мм (ширина) x 158мм (высота)
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div><?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>