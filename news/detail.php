<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Новости");
?><?if(!CModule::IncludeModule("iblock"))

return; 
?>
<?
$arIBlockElement = GetIBlockElement($_GET['ID']);
//var_dump($arIBlockElement);
?>
<body>
    <div class="page-wrap">
<!-- open .header -->
<!-- end html_open.html-->

<!-- start header.html-->
<header class="header">
	<nav class="header__nav">
		<div class="container">
			<div class="logo">
				<a href="/catalog/">
					<picture>
						<source srcset="<?=SITE_TEMPLATE_PATH?>/img/logo-small.svg" media="(max-width: 767px)">
						<img src="<?=SITE_TEMPLATE_PATH?>/img/logo.svg" alt="Bullo Farm">
					</picture>
				</a>
			</div>
			
			<div class="header__menu">
				<div class="menu">
					<a href="/catalog/">Каталог ферм</a>
					<a href="/about/">О нас</a>
				</div>

				<div class="phone">
					<span>8 (800) 123 45 67</span>
				</div>
			</div>
		</div>
	</nav>
</header>
<!-- end header.html-->
<!-- start content-single-news.html-->
<section class="single-news">
    <div class="container">
		<a class="back" href="/news/"><img src="<?=SITE_TEMPLATE_PATH?>/img/arrow-b.jpg" width="16" height="16" alt="">Новости</a>
    </div>
    
    <div class="container container_gray">
        <header class="single__header">
            <h2><?=$arIBlockElement['NAME']?></h2>
            <time class="fs13 greytext"><?=$arIBlockElement['DATE_CREATE']?></time>
        </header>

        <div class="single__content">
            <div class="single__content_featured">
                <div class="single__content_img">
                    <img src="<?=CFile::GetPath($arIBlockElement["PREVIEW_PICTURE"])?>" alt="">
                </div>
                <!-- AddToAny BEGIN -->
                <div class="single__content_soc a2a_kit a2a_kit_size_32 a2a_default_style">
                    <a class="a2a_button_vk"></a>
                    <a class="a2a_button_odnoklassniki"></a>
                    <a class="a2a_button_twitter"></a>
                    <a class="a2a_button_facebook"></a>
                </div>
                <!-- AddToAny END -->
            </div>

            <div class="single__content_text">
				<?=$arIBlockElement['DETAIL_TEXT']?>
			</div>
        </div>

        <footer class="single__footer">
            <h3>Еще больше новостей на нашем телеграмм канале</h3>
            <a href="#"><img src="<?=SITE_TEMPLATE_PATH?>/img/telegram-icon.jpg" width="70" height="70" alt=""></a>
        </footer>
    </div>
</section>

<section class="news news_white news_single">
    <div class="container">
        <header class="news__header news__header_single">
            <h3>Ещё новости</h3>
        </header>

        <div class="news__blocks news__blocks_slick news__blocks_single">
		<?
	if(CModule::IncludeModule('iblock')) {
	$num = 0;
    $arSort= Array("NAME"=>"ASC");
    $arSelect = Array();
    $arFilter = Array("IBLOCK_ID" => 4);
 
    $res =  CIBlockElement::GetList($arSort, $arFilter, false, false, $arSelect);

    while($ob = $res->GetNextElement()){

		$num++;
        $arFields = $ob->GetFields();
		if ($arFields["ID"] == $_GET['ID']) :
			continue;
		endif;
		//var_dump($arFields);
		?>
		<acticle class="news__block">
                <div class="article__content">
                    <a class="featured_img" href="/news/detail.php?ID=<?=$arFields['ID']?>">
                        <img src="<?=CFile::GetPath($arFields["PREVIEW_PICTURE"])?>" alt="">
                    </a>
                    <h4 class="article__title">
                        <a href="/news/detail.php?ID=<?=$arFields['ID']?>"><?=$arFields['NAME']?></a>
                    </h4>
                    <p class=" article__text"><?=$arFields['PREVIEW_TEXT']?></p>
                </div>
                <time class="fs13 greytext"><?=$arFields['DATE_CREATE']?></time>
            </acticle>
		<?
		//print_r($arFields['PROPERTY_PROFIT_VALUE']);
		//print_r($arFields['PROPERTY_TKAN_VALUE']);
    }
}
?>


        </div>
    </div>
</section>
<!-- end content-single-news.html--><?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>