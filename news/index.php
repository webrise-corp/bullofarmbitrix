<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Новости");
?><body>
    <div class="page-wrap">
<!-- open .header -->
<!-- end html_open.html-->

<!-- start header.html-->
<header class="header">
	<nav class="header__nav">
		<div class="container">
			<div class="logo">
				<a href="/catalog/">
					<picture>
						<source srcset="<?=SITE_TEMPLATE_PATH?>/img/logo-small.svg" media="(max-width: 767px)">
						<img src="<?=SITE_TEMPLATE_PATH?>/img/logo.svg" alt="Bullo Farm">
					</picture>
				</a>
			</div>
			
			<div class="header__menu">
				<div class="menu">
					<a href="/catalog/">Каталог ферм</a>
					<a href="/about/">О нас</a>
				</div>

				<div class="phone">
					<span>8 (800) 123 45 67</span>
				</div>
			</div>
		</div>
	</nav>
</header>
<!-- end header.html-->
<!-- start content_news.html-->
<main class="main-news">
	<div class="container">
		<h2 class="page__title">Новости</h2>
		<div class="news news_page">
			<div class="news__blocks news__blocks_wrap">
				<?
	if(CModule::IncludeModule('iblock')) {
	$num = 0;
    $arSort= Array("NAME"=>"ASC");
    $arSelect = Array();
    $arFilter = Array("IBLOCK_ID" => 4);
 
    $res =  CIBlockElement::GetList($arSort, $arFilter, false, false, $arSelect);

    while($ob = $res->GetNextElement()){
		$num++;
        $arFields = $ob->GetFields();
		//var_dump($arFields);
		?>
		<acticle class="news__block news__block_border">
                        <div class="article__content">
                            <a class="featured_img" href="/news/detail.php?ID=<?=$arFields['ID']?>">
                                <img src="<?=CFile::GetPath($arFields["PREVIEW_PICTURE"])?>" alt="">
                            </a>
                            <h4 class="article__title">
								<a href="/news/detail.php?ID=<?=$arFields['ID']?>"><?=$arFields['NAME']?></a>
                            </h4>
                            <p class=" article__text"><?=$arFields['PREVIEW_TEXT']?></p>
                        </div>
                        <time class="fs13 greytext"><?=$arFields['DATE_CREATE']?></time>
                    </acticle>
		<?
		//print_r($arFields['PROPERTY_PROFIT_VALUE']);
		//print_r($arFields['PROPERTY_TKAN_VALUE']);
    }
}
?>

            </div>

            <div class="news__more">
            	<button class="news__more_button" type="button">Ещё новости</button>
            </div>
		</div>
	</div>
</main>
<!-- end content_news.html--><?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>